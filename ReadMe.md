# Read Me Assignment 1

**Ben Puhalski** - (2021W) COMP-4433

February 7th, 2021

# Section 1.1

## Problem 1

```c
int floorSqrt(int n) {
    int a, b;
    a = 0;

    //while (true)
    while(1) {
        b = a * a;
        if(b > n) {
            break; //if i^2 is greater than n then i-1 = floor(sqrt(n))
        } else {
            a++;
        }
    }
    return a - 1;
}
```

*Probably not the most efficient but it is a simple solution

## Problem 2

### A

```pseudocode
a = 31415
b = 14142

a = a - b = 31415 - 14142 = 17273
a = a - b = 17273 - 14142 = 3131
b = b - a = 14142 - 3131 = 11011
b = b - a = 11011 - 3131 = 7880
b = b - a = 7880 - 3131 = 4749
b = b - a = 4749 - 3131 = 1618
a = a - b = 3131 - 1618 = 1513
b = b - a = 1618 - 1513 = 105
a = a - b = 1513 - 105 = 1408
a = a - b = 1408 - 105 = 1303
a = a - b = 1303 - 105 = 1198
a = a - b = 1198 - 105 = 1093
a = a - b = 1093 - 105 = 988
a = a - b = 988 - 105 = 883
a = a - b = 883 - 105 = 778
a = a - b = 778 - 105 = 673
a = a - b = 673 - 105 = 568
a = a - b = 568 - 105 = 463
a = a - b = 463 - 105 = 358
a = a - b = 358 - 105 = 253
a = a - b = 253 - 105 = 148
a = a - b = 148 - 105 = 43
b = b - a = 105 - 43 = 62
b = b - a = 62 - 43 = 19
a = a - b = 43 - 19 = 24
a = a - b = 24 - 19 = 5
b = b - a = 19 - 5 = 14
b = b - a = 14 - 5 = 9
b = b - a = 9 - 5 = 4
a = a - b = 5 - 4 = 1
b = b - a = 4 - 1 = 3
b = b - a = 3 - 1 = 2
b = b - a = 2 - 1 = 1
b = b - a = 1 - 1 = 0
b = 0

a = 1

gcd(31415, 14142) = 1
```

### B

Assuming the algorithm is something like

```c
int minGCD(int m, int n) {
	//  t = min(m, n);
    int t = ( (m < n) ? m : n );
    while(1) {
        if(m % t == 0) {
	        if(n % t == 0) {
		        return t;
            }
	    }
        t--;
    }
}
```

Then compared to:

```c
int euclidGCD(int a, int b) {
    while(1) {
        if(b == 0) {
            return a;
        }
        if(a > b) {
            a = a - b;
        } else {
            b = b - a;
        }
    }
}
```

And in the first algorithm we count both remainder comparisons as the same comparison and count it as our limiting factor. We can compare it to euclid's algorithm for each time a '>' comparison happens.

Then for gcd(31415, 14142) in Euclid's algorithm we had about 34 basic operations. In the other gcd algorithm we would have about 14141 basic operations. This would make Euclids algorithm around 416 times faster than the other gcd algorithm. 

## Problem 3

### A

```pseudocode
// pseudocode
fun gcd(a, b):
    while(true):
        if(b == 0): 
            return a
        if(a > b):
            a = a - b
        else:
            b = b - a
```

```c
// C implementation
int euclidGCD(int a, int b) {
    while(1) {
        if(b == 0) {
            return a;
        }
        if(a > b) {
            a = a - b;
        } else {
            b = b - a;
        }
    }
}
```

### B

*

*

*

*

 **INCOMPLETED**

*

*

*

*



# Section 1.2

## Problem 4

```
Person 1 + Person 2 cross
+2 mins
Person 1 returns
+1 mins
Person 3 + Person 4 cross
+10 mins
Person 2 returns
+2 min
Person 1 + Person 2 cross
+2 mins

2+1+10+2+2 = 17 mins
```

Person 3 & 4 are much much slower than the other 2 so they should go across with each other to save time.

## Problem 5

No computer can 'solve' pi because numbers in computers in most all hardware I know of are represented by bits. Therefore, since pi has an infinite number of digits there is no way to calculate pi in terms of a decimal value. 

It can be represented in a computer if you represent all other numbers in terms of pi. Essentially, for example, making one bit (1) or another value equal to pi. 

However, this would make normal integers not able to be 'solved' or accurately represented by the computer since they would technically require an infinite number of bits to represent now.

# Section 1.3

## Problem 6

### A

```java
// a java implementation using integer comparisons
public static int[] ccsort(int[] A) {
    int[] Count = new int[A.length];
    for(int i = 0; i < A.length; i++) {
        Count[i] = 0;
    }
    for(int i = 0; i < A.length-1; i++) {
        for(int j = i+1; j < A.length; j++) {
            if(A[i] < A[j]) {
                Count[j]++;
            } else {
                Count[i]++;
            }
        }
    }
 
    int[] S = new int[A.length];
    for(int i = 0; i < A.length; i++) {
        S[Count[i]] = A[i];
    }
    return S;
}
```

For the array ``[60, 35, 81, 98, 14, 47]`` the function outputs the array``[14, 35, 47, 60, 81, 98]``.

### B

This algorithm is not stable since if comparison is made and they are determined to be equal the algorithm will reverse the order of them since the algorithm gives a counter to Count[i] (the value before j) instead of Count[j] (the value after i) and will end up reversing them in the end

### C

This algorithm is not in-place because it uses the Count[] array to store the counts of every element. It also uses the array S[] to reorder every element in array A[]. This algorithm uses a lot of extra memory to re-order the data.



## Problem 7

### A

<img src="fig\prob7_fig1.png" style="zoom:50%;" />





<img src="fig\prob7_fig2.png" style="zoom:50%;" />

The above is a generalized version of the Konigsberg problem as an undirected graph.

As a graph theory problem. Can prove if you can or cannot travel through all graph edges (a,b,c,d,e,f,g) once and end up on the same entry node.

### B

To visit every node in the graph, we need some point we can start at and then come back do at the end and stop according to the problem. 

<img src="fig\prob7_fig4.png" style="zoom:75%;" />

As we can see according to this above figure, we need to have an even non-zero number of connected edges for it to be possible start at at and end at any node x. 

<img src="fig\prob7_fig3.png" style="zoom:50%;" />

The above is a diagram of the same graph but has the degrees of each node labeled. As we can see, the graph has no nodes that have an even degreed vertex and therefore cannot have a path/stroll that would visit every node only using each of the edges once and returning to the starting node. 



<img src="fig\prob7_fig6.png" style="zoom: 80%;" />

For any other node than the starting/end node **y**, we need to also have an even number of connected edges to be possible for us to enter and then leave the vertex to continue the path/stroll.

<img src="fig\prob7_fig5.png" style="zoom: 50%;" />

Since all of the vertexes in our original graph had an odd degree, we need to add a minimum of 2 bridges to make the problem possible since one edge can only add to 2 nodes degree by 1. 

So this path/stroll is possible if we add, for example, bridges h and g.

We can translate this back to how it would look in terms of the original figure. 

<img src="fig\prob7_fig7.png" style="zoom: 50%;" />

# Section 1.4

## Problem 8

### A

An actual array in memory should be represented as:

<img src="fig\prob8_fig1.png" style="zoom: 67%;" />

All values should be consecutive to make accessing specific indexes very efficient and not depend on the length of the array.

Therefore, if we want to delete an element from this array we have to affect the other elements in the array. **There is no way to delete an element i in this type of array, without changing how other operations work on this array** since arrays should always be represented as being consecutive

<img src="fig\prob8_fig2.png" style="zoom: 67%;" />

The only way I can think of doing this is by changing how accessing the elements in the array functions because in memory it would have to be represented as something like this:

<img src="fig\prob8_fig3.png" style="zoom:50%;" />

For example, accessing elements of an index greater than or equal to the index you just deleted would have to be accessed like:

```pseudocode
if(x < i) {
	array[x] = array[x]
} else {
	array[x] = array[x+1]
}
```

However, if your array is not represented like this but is instead represented as a sort of [dynamic array](https://en.wikipedia.org/wiki/Dynamic_array) then the operation is much more simple.

<img src="fig\prob8_fig4.png" style="zoom:75%;" />

You can now just:

1. Delete the element
2. Change the pointer to point to the next element 
3. Delete the not needed pointers

### B

Deleting the ith element of an array is the same as deleting the ith element of a sorted array because in both operations the remaining array retains it's order. Therefore, refer to the above (problem 8.a).

## Problem 9

### A

<img src="fig\prob9_fig1.png" style="zoom:50%;" />

### B

<img src="fig\prob9_fig2.png" style="zoom:50%;" />



# Section 2.1

## Problem 10

### A

n(n+1) = n<sup>2</sup> + n

So, the pair has an equal order of growth (to within a constant multiple)

O(n(n+1)) = O(2000n<sup>2</sup>)

### B

θ(100n<sup>2</sup> ) > θ(0.01n<sup>3</sup>)

### C

```

	      log2 (n)
lim       ---------- 
n->inf    ln (n)

 
             1
           -----
           nln(2)
= lim     --------
  n->inf     1
             -
             n
             
             
            1 
= lim      --- 
  n->inf   ln2
  
   1
= --- = 1.442...    
  ln2
```

So, the pair has an equal order of growth (to within a constant multiple)

O(log<sub>2</sub>n) = O(ln(n)) 

### D

```

	      ( log2(n) )^2
lim       ---------- 
n->inf    log2(n^2)

 
             log2(n)^2
= lim        ------
  n->inf  2* log2(n)
             
 
//Common factor 
 
          log2(n) 
= lim      --- 
  n->inf    2
  
= inf
```

So, the function on top will have a higher order of growth than the bottom

θ(log<sub>2</sub><sup>2</sup>n) > θ(log<sub>2</sub>n<sup>2</sup>) 

### E

The pair has an equal order of growth (to within a constant multiple)

O(2<sup>n-1</sup>) = O(2<sup>n</sup>) 

### F

θ((n-1)!) < θ(n!) 

# Section 2.2

## Problem 11

*Assuming lg is a typo for log

*also assuming 5lg(n+100)<sup>10</sup> = 5log((n+100)<sup>10</sup>) and not 5(log(n+100))<sup>10</sup>

The order from top (least) to bottom (most):

log(n<sup>10</sup>)  

(logn)<sup>2</sup>

n<sup>1/3</sup> 

n<sup>4</sup>

3<sup>n</sup>

4<sup>n</sup>

n!

---------

5log(n+100)<sup>10</sup>

ln<sup>2</sup>n

∛n

0.001n<sup>4</sup> + 3n<sup>3</sup>+1

3<sup>n</sup>

2<sup>2n</sup>

(n-2)!

## Problem 12

### A

Since t(n) belongs to O(g(n)) then this means that for the function t(n), g(n) must be the upper bound as n increases to infinity. This means that the reverse must be true. Which in this case means that t(n) is the lower bound for g(n) as n increases to infinity

Therefore, g(n) ∈ Ω(t(n)), which makes the assertion **true**.

### B

Using the formal definition of Θ, if we prove that a function f is bounded by both O(h(n)) and Ω(h(n)) then f is bounded by Θ(h(n)).

For Big O:

O(αg(n)) = O(g(n)), because by the definition of big O, this is true since alpha is a positive constant.

For Big Omega:

Ω(αg(n)) = Ω(g(n)), because by the definition of big theta, this is true since alpha is a positive constant.

So, since it is true for both big O and big omega it can also be true for big theta. This makes the assertion **true**.

### C

Since Θ(g(n)) is the upper and lower bound for g(n) then it is equal to say that it is upper bound 

(**O(g(n))**) and is lower bound and upper bound (**Ω(g(n))**). Therefore the assertion by definition is **true**.

### D

 This assertion is **false** because since the functions are defined on the set of integers I am allowed to define the functions as:

if t(n) = n! when n is odd and t(n) = n when n is even and

if g(n) = n when n is odd and g(n) = n! when n is even.

And since as n grows one never bounds the other it cannot be true that either t(n) ∈ O(g(n)) or t(n) ∈ Ω(g(n)), or both. 

## Problem 13

1. We divide the group of coins into 3 equal groups (if there are 1 or 2 leftover leave them aside)
2. Compare the weights of the groups using the scale. 2 groups should be equal. one should be greater than or less than the other 2 groups
   1. If the other group is greater than the other 2, **the coin is heavier**
   2. If it is less than, **the coin is lighter**
3. If they are all equal then the fake coin is in the 1 or 2 leftover coins.
4. If you have 1 coin leftover, **it is fake**, compare to any other coin to see if it is heavier
5. if you have 2 coins leftover, then add another coin and make each coin your equal group and **repeat step 2 to find the fake coin.**

The endpoints of the algorithm are bolded.

## Problem 14

Since the wall is of infinite length we have to consider exponential growth because for sufficiently high number of steps n, if we change direction at times too early, as n grows for higher distances, the amount that our steps increases will go up more than linearly.

For this algorithm x = 0,1,2,3,4.. and x starts at 0

1. walk 2<sup>x</sup> steps to the right (if door is not found continue to step 2)
2. Go back to the initial point (2<sup>x</sup> steps)
3. walk 2<sup>x</sup> steps to the left (if door is not found continue to step 4)
4. Go back to the initial point (2<sup>x</sup> steps)
5. x = x+1
6. continue until the door is found



For each step of x we talk a total of 2<sup>x</sup> right + 2<sup>x</sup> left + 2<sup>x</sup> + left + 2<sup>x</sup> right. For a total of 4 * 2<sup>x</sup> steps per step.

if P is the final phase we end on the total amount of steps we can possibly take with this algorithm will be:

​      p-1

4 * Σ(2<sup>x</sup>)       + 3 * 2<sup>P</sup>  

​      x=0



Since on our last look for the door we do not go to step 4 but end on step 3, we go 3 * 2<sup>x</sup> instead of 4, and it is 2<sup>p</sup> because it is the P is our final step.

Using the geometric series sum formula:

= 4(  (1-2<sup>p-1</sup>)  /     (1-2)     )  + 3 * 2<sup>P</sup>

= 4(2<sup>P</sup> - 1) + 3 * 2<sup>P</sup>

= 4(2<sup>P</sup>) - 4 + 3 * 2<sup>P</sup>

= 7(2<sup>P</sup>) - 4

then since n <= 2<sup>P</sup> and we are only multiplying it by a constant factor 7, then the algorithm is at most O(n) steps.

## Problem 15

### A

I don't remember linear algebra but it is checking if the matrix's elements are equal from the top right vs the bottom left. Essentially symmetry.

![](D:\Documents\University\Algorithms\assignment_one\prob15_fig1.png)

The algorithm does not look at the diagonal but does check if the other elements are 'symmetric' on it. In this example, does 0 = 0, does 1 = 1, does 2 = 2, etc..

### B

This algorithms basic operation is the comparison

### C

The comparison is executed 2n-3 times. 

### D

The efficiency of this algorithm is in Θ(n)

### E

There is not a more efficient algorithm for this computation if your most expensive computation is comparisons because we check each element only when they are needed the minimum number of times. For any random input matrix A of real numbers this is the most efficient algorithm for the worst case of matrix A where all numbers are the are equal besides the pair A[n-2,n-1] and A[n-1, n-2] or in the case they are all equal. 

## Problem 16

(   (2n-1)<sup>2</sup> + 1    )   /  2

## Problem 17

### A

x(n) = 5(n-1)

### B

3<sup>n-1</sup>*4

### C

x(n) = n<sup>2</sup>