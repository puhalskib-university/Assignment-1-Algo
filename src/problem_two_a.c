#include <stdio.h>
#include <conio.h>

int euclidGCD(int a, int b) {
    while(1) {
        if(b == 0) {
            return a;
        }
        if(a > b) {
            printf("a = a - b = %d - %d = %d\n", a, b, (a-b));
            a = a - b;
        } else {
            printf("b = b - a = %d - %d = %d\n", b, a, (b-a));
            b = b - a;
        }
    }
}

int main() {
    printf("Enter a two positive integers: ");
    int a, b;
    scanf("%d", &a);
    scanf("%d", &b);

    printf("gcd(%d, %d) = %d\n", a, b, euclidGCD(a, b));

    getch();
    return 0;
}