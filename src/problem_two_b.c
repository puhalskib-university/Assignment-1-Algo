#include <stdio.h>
#include <conio.h>

int minGCD(int m, int n) {

    int t = ( (m < n) ? m : n );
    while(1) {
        if(m % t == 0) {
	        if(n % t == 0) {
		        return t;
            }
	    }
        t--;
    }
}

int main() {
    printf("Enter a two positive integers: ");
    int a, b;
    scanf("%d", &a);
    scanf("%d", &b);

    printf("gcd(%d, %d) = %d\n", a, b, minGCD(a, b));

    getch();
    return 0;
}