import java.util.Arrays;

//For this question I used java because VLAs are not allowed in C

public class problem_six {

    public static int[] ccsort(int[] A) {
        int[] Count = new int[A.length];
        for(int i = 0; i < A.length; i++) {
            Count[i] = 0;
        }
        for(int i = 0; i < A.length-1; i++) {
            for(int j = i+1; j < A.length; j++) {
                if(A[i] < A[j]) {
                    Count[j]++;
                } else {
                    Count[i]++;
                }
            }
        }

        int[] S = new int[A.length];
        for(int i = 0; i < A.length; i++) {
            S[Count[i]] = A[i];
        }
        return S;
    }
    public static void main(String args[]) {
        int[] a = {60, 35, 81, 98, 14, 47};

        System.out.println("\t" + Arrays.toString(a));
        System.out.println("sorted:\t" + Arrays.toString(ccsort(a)));
    }
}
