#include<stdio.h>
#include <conio.h>

int floorSqrt(int n) {
    int a, b;
    a = 0;

    while(1) {
        b = a * a;
        if(b > n) {
            break;
        } else {
            a++;
        }
    }
    return a - 1;
}

int main() {
    printf("Enter a positive integer: ");
    int n;
    scanf("%d", &n);

    printf("floor(sqrt(%d)) = %d\n", n, floorSqrt(n));

    getch();
    return 0;
}